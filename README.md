<h2> Formal languages and automata theory </h2>
Source for Turing Machine that recognizes the Fibonacci sequence from the Formal Languages and Automata Theory course at Universidad Carlos III de Madrid.

<p></p>

Feel free to view, use and redistribute these files in any way.


This was done in 2016 using <a href="http://www.jflap.org/">JFLAP</a>.

<h3>Turing machine </h3>
The Turing machine (from now on, TM) is made of 8 subTMs. <br>
Input format is <code>n$f(0)$f(1)</code> and output is <code>f(n)</code> without leading 0s. In the following subsections these TMs will be explained in detail.

<h5> F(0) and f(1) recognizer (<code>reconocedor_f0_f1</code>)</h5>
In the initial state, the TM is on this subTM. It recognizes the first two numbers of the sequence which are in the input. <br>
It goes through the initial N and takes an A-path if it is asked for f(0) and a B-path if it is asked for f(1). If the value of N is greater than 1, the TM will continue until <code>resta_contador</code>. <br>
A-path goes from the beginning until the first <code>$</code> without finding 1s and when the TM reaches that state, it removes every value to the left until it reaches a blank space. Then it moves from left to right until the first value of f(0), removes the leading 0s and reaches the subTM final state.
<br>
B-path also seeks until <code>$</code> and if the subTM found only one 1, it will delete everything before the first value of f(1) until blank space is found. Then removes leading 0s and reaches final state of subMT. <br>
If N is not equal to 0 or 1, the subTM moves to the next subTM, <code>resta_contador</code>.

<h5> Counter decrement (<code>resta_contador</code>) </h5>
This subTM decrements the counter by 1. If after the decrement the counter is zero, the TM will reach <code>final</code> subTM. Otherwise, it will go through f(0) until the last number and will get to <code>suma_nums</code>.

<h5> Number addition (<code>suma_nums</code>) </h5>
This subTM adds the f(n-2) and f(n-1) numbers and stores the result right of both. This result is mirrored, which means that the most significant number is at the right, and the less significant is at the left. <br>
Then we swap both numbers for Xs, and every bit on the sum for A or B (if it's 1 or 0). If there's a carry-over number, we write an asterisk (<code>suma_aux</code>) <br>
After we are done with the addition, we store a slash (/) after the sum and start mirroring the number (go to <code>reflejar_res</code>).

<h5> Sum mirroring (<code>reflejar_res</code>) </h5>
This subTM mirrors the result after the slash (/). It starts at the less significant bit of the result, and it swaps every bit for a X and stores the number right of the slash (/). The subTM loops until the last number is mirrored and swaps the hyphen (-) for a $ so we finally have the real number.

<h5> Rewind (<code>rebobinar</code>) </h5>
This subTM just moves the pointer to the beginning of the input and goes to <code>resta_contador</code>.

<h5> Final subTM (<code>final</code>) </h5>
This subTM ends the recognition. Being at the leftmost of the input, it goes until the rightmost <code>$</code> and deletes everything at the left. Then the subTM removes the leading 0s and ends the process.